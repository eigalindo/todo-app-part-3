import React, { Component } from "react";
import { Route, NavLink } from "react-router-dom";
import "./index.css";
import TodoList from "./TodoList";
import { connect } from "react-redux";
import { clearCompletedTodos, addTodo, toggleTodo, removeTodo } from "./actions"

class App extends Component {
  state = {
    value: ""
  };

  handleChange = (event) => {
    this.setState({ value: event.target.value })
  }
  handleCreateTodo = (event) => {
    if (event.key === "Enter") {
      this.props.addTodo(this.state.value)
      this.setState({ value: "" })
    }
  }
  handleDeleteTodo = (todoIdToDelete) => (event) => {
    this.props.removeTodo(todoIdToDelete)
  }

  toggleCompleted = (todoIdToToggle) => (event) => {
    this.props.toggleTodo(todoIdToToggle)
  }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route path="/" exact render={() => <TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.props.todos} toggleCompleted={this.toggleCompleted} />}></Route>
        <Route path="/active" render={() => <TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.props.todos.filter(todo => todo.completed === false)} toggleCompleted={this.toggleCompleted} />}></Route>
        <Route path="/completed" render={() => <TodoList handleDeleteTodo={this.handleDeleteTodo} todos={this.props.todos.filter(todo => todo.completed === true)} toggleCompleted={this.toggleCompleted} />}></Route>
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>todos={this.props.todos.filter(todo => todo.completed === false).length}</strong> item(s) left
  </span>
          <ul className="filters">
            <li>
              <NavLink exact activeClassName="selected" to="/">All</NavLink>
            </li>
            <li>
              <NavLink activeClassName="selected" to="/active">Active</NavLink>
            </li>
            <li>
              <NavLink activeClassName="selected" to="/completed">Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={this.props.clearCompletedTodos}>Clear completed</button>
        </footer>
      </section>
    );
  }
}
export default connect((state) => {
  return {
    todos: state.todos
  }
}, { clearCompletedTodos, addTodo, toggleTodo, removeTodo })(App);
