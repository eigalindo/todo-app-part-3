export const CLEAR_COMPLETED_TODOS = "CLEAR_COMPLETED_TODOS";
export const clearCompletedTodos = () => {
    return { type: CLEAR_COMPLETED_TODOS }
}
export const ADD_TODO = "ADD_TODO"
export const addTodo = (title) => {
    return { type: ADD_TODO, payload: title }
}
export const TOGGLE_TODO = "TOGGLE_TODO"
export const toggleTodo = (id) => {
    return { type: TOGGLE_TODO, payload: id}
}
export const REMOVE_TODO = "REMOVE_TODO"
export const removeTodo = (id) => {
    return { type: REMOVE_TODO, payload: id}
}