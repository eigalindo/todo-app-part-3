import todosList from "./todos.json"
import { CLEAR_COMPLETED_TODOS, ADD_TODO, TOGGLE_TODO, REMOVE_TODO } from "./actions.js";

const initialState = {
    todos: todosList
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case REMOVE_TODO:
                const newTodos4 = state.todos.slice();
                const todoIndexToDelete = newTodos4.findIndex(todo => {
                  if (todo.id === action.payload) {
                    return true;
                  }
                  return false;
                })
                newTodos4.splice(todoIndexToDelete, 1);
                return { ...state, todos: newTodos4 };

        case TOGGLE_TODO:
            const newTodos3 = state.todos.map(toDo => {
                if (toDo.id === action.payload) {
                    toDo.completed = !toDo.completed
                }
                return toDo;

            })
            return { ...state, todos: newTodos3 };
        case CLEAR_COMPLETED_TODOS:
            const newTodos = state.todos.filter(todo => todo.completed !== true);
            return { ...state, todos: newTodos };
        case ADD_TODO:
            const newTodo = {
                userId: 1,
                id: Math.floor(Math.random() * 100000000),
                title: action.payload,
                completed: false,
            };
            const newTodos2 = state.todos.slice();

            newTodos2.push(newTodo)

            return { ...state, todos: newTodos2 };
        default:
            return state;
    }
}

export default reducer;